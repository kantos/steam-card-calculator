from lxml import html
import json
from urllib import request

print("WARNING: your inventory must be public!!!")
print("Nick:"),
login = input()

file = request.urlopen(
    "http://steamcommunity.com/id/%s/inventory/json/753/6/" % login)
j = json.load(file)

sum = 0.0
print("Cards count: %s" % (len(j['rgInventory'])))
for key in j['rgDescriptions']:
    searchStr = j['rgDescriptions'][key]['name'].replace(" ", "+").replace("'s", "")
    classId = j['rgDescriptions'][key]['classid']
    count = 0
    for k in j['rgInventory']:
        if j['rgInventory'][k]['classid'] == classId:
            count = count + 1
    root = html.parse("https://steamcommunity.com/market/search?q=%s" % searchStr)
    price = float(root.xpath("(//div[@class='market_listing_right_cell market_listing_num_listings'])[2]//br")[1].tail.strip()[1:-4])
    sum = sum + price*count
    print("%s x %s = %s %s" % (price, count, price*count, j['rgDescriptions'][key]['name']))

print(sum)
